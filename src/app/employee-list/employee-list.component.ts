import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FetchdataService} from '../fetchdata.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {
  title = 'Employees List';
  @Input() DataList: any = [];
  resultEmpData: any;

  constructor(private fetchData: FetchdataService, private router: Router) {
  }

  ngOnInit() {
    this.resultEmpData = this.fetchData.empData;
  }

}
