import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FetchdataService} from './fetchdata.service';
import {AppRoutingModule, routingComponents} from './app-routing.module';
import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {HeaderComponent} from './header/header.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {EmployeeListComponent} from './employee-list/employee-list.component';
import {DepartmentListComponent} from './department-list/department-list.component';
import {DesignationListComponent} from './designation-list/designation-list.component';
import {DataListComponent} from './data-list/data-list.component';
import {UsersComponent} from './users/users.component';
import {GenericFormsComponent} from './generic-forms/generic-forms.component';
import { StoreComponent } from './store/store.component';

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    HeaderComponent,
    EmployeeListComponent,
    DepartmentListComponent,
    DesignationListComponent,
    DataListComponent,
    UsersComponent,
    GenericFormsComponent,
    StoreComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    FetchdataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
