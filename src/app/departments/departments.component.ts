import { Component, OnInit } from '@angular/core';
import {FetchdataService} from '../fetchdata.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-departments',
  templateUrl: './departments.component.html',
  styleUrls: ['./departments.component.css']
})
export class DepartmentsComponent implements OnInit {
  title = 'Department List';
  FormParts: number = 2;
  resultDepartData: any;
  constructor( private fetchData: FetchdataService, private router: Router) { }

  ngOnInit() {
    this.resultDepartData = this.fetchData.departData;
  }
  getFormList() {
    this.router.navigate(['/departmentlist']);
  }
}
