import {Component, Input, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators, ValidatorFn} from '@angular/forms';

@Component({
  selector: 'app-generic-forms',
  templateUrl: './generic-forms.component.html',
  styleUrls: ['./generic-forms.component.css']
})
export class GenericFormsComponent implements OnInit {

  myForm: FormGroup;
  @Input('fields') fields: Array<GenericFormField> = [];

  constructor(private formbuilder: FormBuilder) {
  }

  ngOnInit() {
    this.getForm();
  }

  getForm(){
    this.myForm = this.formbuilder.group({});

    for (let field of this.fields) {
      console.log(field);
      this.myForm.addControl(
        field.name,
        this.formbuilder.control('', field.validators)
      );
    }
  }
}

export class GenericFormField {
  title: string;
  name: string;
  id: string;
  type: 'text' | 'number' | 'email' | 'date' | 'checkbox' | 'select' | 'radio';
  validators?: Array<ValidatorFn>;
  choose?: Array<Lookup>;
}

export class Lookup {
  key: string;
  value: string;
  selected?: boolean;
}
