import {Component, OnInit} from '@angular/core';
import {FetchdataService} from '../fetchdata.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {
  employees;
  title = 'Employees';
  ApiType = 'employees';
  DataOpject: { [id: string]: any } = {};
  Name: any;
  FatherName: any;
  DOB: any;
  Gender: any;
  Email: any;
  Contact: any;
  Address: any;
  Depart: any;
  Desig: any;
  resultDepartData: any;
  resultDesigData: any;
  DataList: any;

  constructor(private fetchData: FetchdataService, private router: Router) {
    this.fetchData.getData(this.ApiType).subscribe((data) => {
      this.DataList = data;
    }, (error) => console.log('error:' + error));
  }

  ngOnInit() {
    this.resultDepartData = this.fetchData.departData;
    this.resultDesigData = this.fetchData.desigData;
  }

  getFormList() {
    this.router.navigate(['/employeelist']);
  }

  addEmployee() {
    this.DataOpject = {
      name: this.Name,
      father_name: this.FatherName,
      dob: this.DOB,
      gender: this.Gender,
      email: this.Email,
      contact: this.Contact,
      address: this.Address,
      depart_name: this.Depart,
      desig_name: this.Desig
    };
    this.fetchData.addData(this.DataOpject, this.ApiType).subscribe(data => console.log(data));
  }

  updateEmployee() {
    this.DataOpject = {
      name: this.Name,
      father_name: this.FatherName,
      dob: this.DOB,
      gender: this.Gender,
      email: this.Email,
      contact: this.Contact,
      address: this.Address,
      depart_name: this.Depart,
      desig_name: this.Desig
    };
    this.fetchData.updateData(1, this.DataOpject, this.ApiType);
  }

  deleteEmployee() {
    this.fetchData.deleteData(1, this.ApiType);
  }
}
