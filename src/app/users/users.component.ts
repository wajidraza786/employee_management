import {Component, OnInit} from '@angular/core';
import {Validators} from '@angular/forms';
import {GenericFormField} from '../generic-forms/generic-forms.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

  genderObj = [
    {key: '1', value: 'Male', selected: true},
    {key: '2', value: 'Female'}
  ];
  userTypeObj = [
    {key: '1', value: 'Administrator', selected: true},
    {key: '2', value: 'user'}
  ];

  fields: Array<GenericFormField> = [
    {name: 'firstName', id: 'firstName', title: 'First Name', type: 'text', validators: [Validators.required]},
    {name: 'lastName', id: 'lastName', title: 'Last Name', type: 'text', validators: [Validators.required]},
    {name: 'gender', id: 'gender', title: 'Gender', type: 'select', choose: this.genderObj},
    {name: 'age', id: 'age', title: 'Age', type: 'number'},
    {name: 'email', id: 'email', title: 'Email', type: 'email'},
    {name: 'date', id: 'date', title: 'Date', type: 'date'},
    {name: 'category', id: 'category', title: 'Category', type: 'select', choose: this.userTypeObj},
    {name: 'status', id: 'status', title: 'Status', type: 'checkbox'},
  ];

  formGroups = [
    {name: 'first-group'}
  ];
}
