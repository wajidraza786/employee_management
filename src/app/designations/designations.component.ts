import { Component, OnInit } from '@angular/core';
import {FetchdataService} from '../fetchdata.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-designations',
  templateUrl: './designations.component.html',
  styleUrls: ['./designations.component.css']
})
export class DesignationsComponent implements OnInit {
  title = 'Designation List';
  FormParts: number = 2;
  resultDesigData: any;
  constructor( private fetchData: FetchdataService, private router: Router) { }

  ngOnInit() {
    this.resultDesigData = this.fetchData.desigData;
  }
  getFormList() {
    this.router.navigate(['/designationlist']);
  }
}
