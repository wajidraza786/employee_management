import {Component, Input, OnInit} from '@angular/core';
import {FetchdataService} from '../fetchdata.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-data-list',
  templateUrl: './data-list.component.html',
  styleUrls: ['./data-list.component.css']
})
export class DataListComponent implements OnInit {
  @Input() DataListHeading: any [] = [];
  @Input() type: any;
  DataList: any;

  constructor(
    private fetchData: FetchdataService,
    private router: Router) {
  }

  getForm() {
    this.router.navigate(['/' + this.type]);
  }

  getListData() {
    this.fetchData.getData(this.type).subscribe(data => {
      this.DataList = data;
    });
  }

  ngOnInit() {
    this.getListData();
  }
}
