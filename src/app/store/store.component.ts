import {Component, OnInit} from '@angular/core';
import {GenericFormField} from '../generic-forms/generic-forms.component';
import {Validators} from '@angular/forms';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

  fields: Array<GenericFormField> = [
    {name: 'firstName', id: 'firstName', title: 'First Name', type: 'text', validators: [Validators.required]},
    {name: 'lastName', id: 'lastName', title: 'Last Name', type: 'text', validators: [Validators.required]},
    {name: 'age', id: 'age', title: 'Age', type: 'number'},
    {name: 'email', id: 'email', title: 'Email', type: 'email'},
    {name: 'date', id: 'date', title: 'Date', type: 'date'},
  ];

  formGroups = [
    {name: 'first-group'}
  ];
}
