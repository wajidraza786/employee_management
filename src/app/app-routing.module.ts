import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {EmployeesComponent} from './employees/employees.component';
import {DepartmentsComponent} from './departments/departments.component';
import {DesignationsComponent} from './designations/designations.component';
import {PostsComponent} from './posts/posts.component';
import {PostComponent} from './post/post.component';
import {GallaryComponent} from './gallary/gallary.component';
import {EmployeeListComponent} from './employee-list/employee-list.component';
import {DepartmentListComponent} from './department-list/department-list.component';
import {DesignationListComponent} from './designation-list/designation-list.component';
import {DataListComponent} from './data-list/data-list.component';
import {UsersComponent} from './users/users.component';
import {StoreComponent} from './store/store.component';

const routes: Routes = [
  {path: 'employees', component: EmployeesComponent},
  {path: 'employeelist', component: EmployeeListComponent},
  {path: 'departments', component: DepartmentsComponent},
  {path: 'departmentlist', component: DepartmentListComponent},
  {path: 'designations', component: DesignationsComponent},
  {path: 'designationlist', component: DesignationListComponent},
  {path: 'posts', component: PostsComponent},
  {path: 'posts/:id', component: PostComponent},
  {path: 'photos', component: GallaryComponent},
  {path: 'list/:type', component: DataListComponent},
  {path: 'users', component: UsersComponent},
  {path: 'store', component: StoreComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

export const routingComponents = [
  EmployeesComponent,
  DepartmentsComponent,
  DesignationsComponent,
  PostsComponent,
  PostComponent,
  GallaryComponent
];
