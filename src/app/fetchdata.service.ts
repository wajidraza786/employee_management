import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FetchdataService {
  public apilink: any = 'http://127.0.0.1:8000/';
  public empData = [];
  public departData = [];
  public desigData = [];

  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) {
    fetch('http://127.0.0.1:8000/employees/')
      .then((res) => res.json())
      .then((data) => this.empData.push(data))
      .catch((err) => console.log(err));

    fetch('http://127.0.0.1:8000/departments/')
      .then((res) => res.json())
      .then((data) => this.departData.push(data))
      .catch((err) => console.log(err));

    fetch('http://127.0.0.1:8000/designations/')
      .then((res) => res.json())
      .then((data) => this.desigData.push(data))
      .catch((err) => console.log(err));
  }

  getData(type): Observable<any> {
    console.log(this.apilink + type);
    return this.http.get<any>(this.apilink + type);
  }

  addData(employees, type): Observable<any> {
    console.log(this.apilink + type);
    return this.http.post<any>(this.apilink + type, employees, this.httpOptions);
  }

  updateData(id: number, employees, type): Observable<any> {
    console.log(this.apilink + type + '/' + id);
    return this.http.put<any>(this.apilink + type + '/' + id, employees, this.httpOptions);
  }

  deleteData(id: number, type): Observable<any> {
    console.log(this.apilink + type + '/' + id);
    return this.http.delete<any>(this.apilink + type + '/' + id, this.httpOptions);
  }

}
